﻿using Application_3.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application_3.DTO.Movie
{
    public class MovieReadDTO
    {
        public int Id { get; set; }
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        public int ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        // Navigation property to franchise
        public int FranchiseId { get; set; }
        // Navigation property to characters
        public List<int> Characters { get; set; }
    }
}
