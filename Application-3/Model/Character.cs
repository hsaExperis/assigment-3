﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Application_3.Model
{
    public class Character
    {
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string FullName { get; set; }
        [StringLength(100)]
        public string Alias { get; set; }
        [StringLength(10)]
        public string Gender { get; set; }
        [StringLength(500)]
        public string Picture { get; set; }

        public ICollection<Movie> Movies { get; set; }
    }
}
