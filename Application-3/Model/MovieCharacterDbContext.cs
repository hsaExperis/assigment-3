﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application_3.Model
{
    public class MovieCharacterDbContext : DbContext
    {
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        /*protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=localhost\SQLEXPRESS;Initial Catalog=MovieCharacterDb;Integrated Security=True;");
        }*/

        public MovieCharacterDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Franchises
            modelBuilder.Entity<Franchise>().HasData(new Franchise 
            { 
                Id = 1,
                Name = "The Lord of the Rings", 
                Description = "The Lord of the Rings is an epic high fantasy novel by the English author and scholar J. R. R. Tolkien. " +
                "Set in Middle-earth, the world at some distant time in the past, the story began as a sequel to Tolkien's 1937 children's book The Hobbit, but eventually developed into a much larger work. " +
                "Written in stages between 1937 and 1949, The Lord of the Rings is one of the best-selling books ever written, with over 150 million copies sold. " +
                "The title names the story's main antagonist, the Dark Lord Sauron, who had in an earlier age created the One Ring to rule the other Rings of Power as the ultimate weapon in his campaign to conquer and rule all of Middle-earth. " +
                "From homely beginnings in the Shire, a hobbit land reminiscent of the English countryside, the story ranges across Middle-earth, following the quest to destroy the One Ring mainly through the eyes of the hobbits Frodo, Sam, Merry and Pippin. "
            });
            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 2,
                Name = "Star Wars",
                Description = "Star Wars is an American epic space opera multimedia franchise created by George Lucas, which began with the eponymous 1977 film and quickly became a worldwide pop-culture phenomenon. The franchise has been expanded into various films and other media, including television series, video games, novels, comic books, theme park attractions, and themed areas, comprising an all-encompassing fictional universe. In 2020, its total value was estimated at US$70 billion, and it is currently the fifth-highest-grossing media franchise of all time."
            });
            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 3,
                Name = "Marvel Cinematic Universe",
                Description = "The Marvel Cinematic Universe (MCU) is an American media franchise and shared universe centered on a series of superhero films produced by Marvel Studios. The films are based on characters that appear in American comic books published by Marvel Comics. The franchise also includes television series, short films, digital series, and literature. The shared universe, much like the original Marvel Universe in comic books, was established by crossing over common plot elements, settings, cast, and characters. "
            });

            // Movies
            modelBuilder.Entity<Movie>().HasData(new Movie 
            { 
                Id = 1,
                MovieTitle = "The Fellowship of the Ring",
                ReleaseYear = 2001,
                Director = "Peter Jackson",
                Genre = "Fantasy, Adventure",
                FranchiseId = 1
            });

            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 2,
                MovieTitle = "The Two Towers",
                ReleaseYear = 2002,
                Director = "Peter Jackson",
                Genre = "Fantasy",
                FranchiseId = 1
            });

            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 3,
                MovieTitle = "Star Wars: Episode IV - A New Hope",
                ReleaseYear = 1977,
                Director = "George Lucas",
                Genre = "Sci-Fi, Fantasy, Adventure",
                FranchiseId = 2
            });

            // Characters
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 1,
                FullName = "Frodo Baggins",
                Alias = "Mr Frodo",
                Gender = "Male"
            });

            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 2,
                FullName = "Gandalf the Grey",
                Alias = "Mithrandir",
                Gender = "Male"
            });

            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 3,
                FullName = "Luke Skywalker",
                Gender = "Male"
            });

            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 4,
                FullName = "Princess Leia Organa",
                Alias = "Leia",
                Gender = "Female"
            });


            modelBuilder.Entity<Character>()
                .HasMany(p => p.Movies)
                .WithMany(l => l.Characters)
                .UsingEntity<Dictionary<string, object>>(
                "CharacterMovie",
                c => c.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                m => m.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                jt =>
                {
                    jt.HasKey("MovieId", "CharacterId");
                    jt.HasData(
                        new { MovieId = 1, CharacterId = 1 },
                        new { MovieId = 1, CharacterId = 2 }, 
                        new { MovieId = 2, CharacterId = 3},
                        new { MovieId = 2, CharacterId = 4 }
                        );
                });

        }
    }
}
