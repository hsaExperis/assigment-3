﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Application_3.Model;
using AutoMapper;
using Application_3.DTO.CharacterDTO;
using System.Net.Mime;

namespace Application_3.Controllers
{
    [Route("api/v1/characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all characters
        /// </summary>
        /// <returns>List of all Character objects</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _context.Characters
                .Include(c => c.Movies)
                .ToListAsync());
            
        }

        /// <summary>
        /// Gets a specific character object by id
        /// </summary>
        /// <param name="id">int, id of character object</param>
        /// <returns>Character object</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map< CharacterReadDTO>(character);
        }

        /// <summary>
        /// Updates an existing character object by id
        /// </summary>
        /// <param name="id">int, id of character object</param>
        /// <param name="dtoCharacter">The character object</param>
        /// <returns>No content</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO dtoCharacter)
        {
            if (id != dtoCharacter.Id)
            {
                return BadRequest();
            }

            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);

            _context.Entry(domainCharacter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Adds a new character object
        /// </summary>
        /// <param name="dtoCharacter">CharacterCreateDTO, Character to be added</param>
        /// <returns>Added character</returns>
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(CharacterCreateDTO dtoCharacter)
        {
            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);
            _context.Characters.Add(domainCharacter);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCharacter", new { id = domainCharacter.Id }, _mapper.Map<CharacterReadDTO>(domainCharacter));
        }

        /// <summary>
        /// Deletes an existing character
        /// </summary>
        /// <param name="id">int, id of character to be deleted</param>
        /// <returns>No content</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.Id == id);
        }

        /// <summary>
        /// Updates relationship between character and movies
        /// </summary>
        /// <param name="id">int, character id</param>
        /// <param name="movies">List of movies, movies to be altered</param>
        /// <returns>No content</returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateCharacterMovie(int id, List<int> movies)
        {
            if (!CharacterExists(id))
            {
                return NotFound();
            }

            Character characterToUpdateMovies = await _context.Characters
                .Include(c => c.Movies)
                .Where(c => c.Id == id)
                .FirstAsync();

            List<Movie> characterMovies = new();
            foreach (int  movieId in movies)
            {
                Movie movie = await _context.Movies.FindAsync(movieId);
                if (movie == null)
                {
                    return BadRequest("Movie doesnt exist");
                }
                characterMovies.Add(movie);
            }

            characterToUpdateMovies.Movies = characterMovies;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }
    }
}
