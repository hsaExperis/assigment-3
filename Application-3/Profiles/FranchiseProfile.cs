﻿using Application_3.DTO.FranchiseDTO;
using Application_3.Model;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application_3.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(m => m.Id).ToArray()));

            CreateMap<FranchiseEditDTO, Franchise>();
            CreateMap<FranchiseCreateDTO, Franchise>();
        }
    }
}
