﻿using Application_3.DTO.CharacterDTO;
using Application_3.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace Application_3.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(c => c.Id).ToArray()));

            CreateMap<CharacterEditDTO, Character>();

            CreateMap<CharacterCreateDTO, Character>();
        }
    }
}
